package oop2;

public class Employees extends Person {
	float salary;

	public Employees(int id, String Name, float salary) {
		
		super (id,Name);
		this.salary = salary;
		
	}
	
	public void display() {
		
		System.out.println(id + " " + Name + " " + salary);
	}
}
