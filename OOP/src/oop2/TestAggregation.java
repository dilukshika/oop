package oop2;

public class TestAggregation {

	public static void main (String args []) {
		Address address1 = new Address("Jaffna", "North", "Sri Lanka");
		Address address2 = new Address("Colombo", "South", "Sri Lanka");
		
		Employee e = new Employee(111, "ABD", address1);
		Employee e2 = new Employee(112, "MSD", address2);
		
		e.display();
		e2.display();
	}
}
