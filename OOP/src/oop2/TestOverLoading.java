package oop2;

public class TestOverLoading {

	public static void main (String args []) {
		System.out.println(Adder.add(12, 13));
		System.out.println(Adder.add(12.3, 13.5));
		System.out.println(Adder.add(12, 13, 15));
		
	}
}
