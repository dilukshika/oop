package oop1;

public class Student {
	
	int rollno;
	String name;
	static int batchNo;
	
	
	Student (){  // default constructor or no argument constructor
		rollno=20;
		name="ABC";
	}
	
	Student (int rollno,String name){  // paramicterised constructor
		this.rollno=rollno;
		this.name=name;
	}
	
	public static void main (String [] args) {
		Student st1 =new Student(15,"Dilu");
		Student st2 =new Student(20,"Keerthu");
		Student st3 =new Student();
		
		
	/*	
		st1.rollno = 15;
		st1.name= "Dilukshi";
		st1.batchNo=15;
	*/
		
		System.out.println(st1.name);
		System.out.println(st1.rollno);
		System.out.println(st1.batchNo);
		System.out.println(st2.name);
		System.out.println(st2.rollno);
		System.out.println(st2.batchNo);
		System.out.println(st3.name);
		System.out.println(st3.rollno);
		
				}

}
