package Interface;

public class Rectangle implements Drawable, Printable {

	public void draw() {

		System.out.println("drawing rectangle");
	}

	public void print() {

		System.out.println("Printing Rectangle");
	}

}
